import 'package:flutter/material.dart';

class aboutPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("About"),
        ),
        body: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(30, 20, 10, 30),
              child: Text('This is a simple app that shows the user information about the listed shows. This version does not have any interactivity to any API or database, so this app only has a mockup of supplied data.',
                textAlign: TextAlign.center,),
            ),            Container(
              padding: EdgeInsets.fromLTRB(30, 30, 10, 30),
              child: Text('All of the information is from the IMDB website for the respective show.\n Please don\'t sue, we don\'t have any money.',
                textAlign: TextAlign.center,),
            ),
          ],
        )
    );
  }
}