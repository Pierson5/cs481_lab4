import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab4/Widgets/showPages/BreakingBad.dart';
import 'showPages/AoT.dart';
import 'showPages/BreakingBad.dart';
import 'showPages/GoT.dart';
import 'showPages/Dark.dart';
import 'AboutPage.dart';

//List of shows and images
class showInfo {
  const showInfo(this.poster, this.infoPage);

  final String poster;
  final Widget infoPage;
}

List<showInfo> shows = [
  showInfo('images/poster/BreakingBad.jpg', BreakingBad()),
  showInfo('images/poster/AoT.jpg', AoT()),
  showInfo('images/poster/GoT.jpg', GoT()),
  showInfo('images/poster/Dark.jpg', Dark()),
];

//Allows string changes when pressing the logout/login button
class accountDetails {
  const accountDetails(
      this.name, this.subtext, this.button, this.image, this.logText);

  final String name;
  final String subtext;
  final String button;
  final String image;
  final String logText;
}

final List<accountDetails> _accounts = <accountDetails>[
  const accountDetails('Winston', 'User Since 2008', 'Email Verified',
      'images/dog.jpg', 'Log Out'),
  const accountDetails('Guest', '', 'Logged Out', 'images/guest.jpg', 'Log In')
];
int accountNum = 0; //Choose from _accounts

//List of Popup Menu Items
enum MenuOption {
  Settings,
}

class HomePage extends StatefulWidget {
  //HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(title: Text('My Shows'),

          //popup menu
          actions: <Widget>[
            PopupMenuButton<MenuOption>(
              itemBuilder: (BuildContext context) {
                return <PopupMenuEntry<MenuOption>>[
                  PopupMenuItem(
                    child: Text('Settings'),
                    value: MenuOption.Settings,
                  ),
                ];
              },
            )
          ]),

      //Grid view tv show layout
      body: GridView.count(
          //Specify how many columns in grid
          crossAxisCount: 2,
          //change the size of the grid squares "width / height"
          childAspectRatio: 250 / 400,
          //How many items to generate in the grid, should be as many as
          // the user specifies (how many watched/watching)
          children: List.generate(4, (index) {
            //Index starts at 0, this will display index starting at 1
            var indexStartAtOne = index + 1;
            //create cards for each item in the grid, these will hold movie
            //photos/posters.
            return new InkWell(
              child: Card(
                //image won't overflow container
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                elevation: 10.0,
                //container for the images/movie posters
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      //adds image from array using index of GridView
                      image: AssetImage(shows[index].poster),
                      fit: BoxFit.fill,
                    ),
                  ),
                  //displays the index number of the card, top left
                  child: Text(
                    "$indexStartAtOne",
                    style: TextStyle(fontSize: 20.0, color: Colors.white),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => shows[index].infoPage),
                );
              },
            );
          })),

      //Side Menu
      drawer: new Drawer(
        //displays menu items as a list
        child: ListView(
          children: <Widget>[
            //menu header
            DrawerHeader(
              //the drawerHeader is at the top and is blue
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
              //header items placed in a stack, one on top of other
              child: Stack(
                children: <Widget>[
                  //widgets for header menu, could contain username,
                  //photo, icon, descriptions, etc...
                  Align(
                    alignment: Alignment.centerLeft,
                    child: CircleAvatar(
                      backgroundImage: AssetImage(_accounts[accountNum].image),
                      radius: 50.0,
                    ),
                  ),
                  //username
                  Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      _accounts[accountNum].name,
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                  Align(
                    //offset by 30%
                    alignment: Alignment.centerRight + Alignment(0, .3),
                    child: Text(
                      _accounts[accountNum].subtext,
                      style: TextStyle(
                        color: Colors.white70,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight + Alignment(0, .8),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.greenAccent),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Text(
                          _accounts[accountNum].button,
                          style: TextStyle(color: Colors.greenAccent),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),

            //menu list items
            new ListTile(
              title: new Text(
                'My Shows',
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              onTap: () {}, //no implementation
            ),

            //divider line for menu items
            new Divider(color: Colors.black, height: 15.0),

            new ListTile(
              title: new Text(
                'Search',
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              onTap: () {}, //no implementation
            ),

            new Divider(color: Colors.black, height: 15.0),

            new ListTile(
              title: new Text(
                'Add Show',
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              onTap: () {}, //no implementation
            ),

            new Divider(color: Colors.black, height: 15.0),

            new ListTile(
              title: new Text(
                'About',
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              onTap: () {Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => aboutPage()),
              );}, //no implementation
            ),

            new Divider(color: Colors.black, height: 15.0),

            new ListTile(
              title: new Text(
                _accounts[accountNum].logText,
                style: TextStyle(fontSize: 20.0, color: Colors.black),
              ),
              onTap: () {
                setState(() {
                  if (accountNum == 0) {
                    accountNum = 1;
                  } else {
                    accountNum = 0;
                  }
                });
              },
            ),
          ],
        ),
      ),
    ));
  }
}
