import 'package:flutter/material.dart';

//Styles for fonts
TextStyle bodyText = TextStyle(
  fontSize: 12,
  color: Colors.black,
);
TextStyle subBodyText = TextStyle(
  fontSize: 12,
  color: Colors.grey.shade700,
  decoration: TextDecoration.underline,
);
TextStyle sectionTitleText = TextStyle(
  fontSize: 25,
  color: Colors.black,
  fontWeight: FontWeight.bold,
);

//Displays images from show
Widget scenesWrap = Wrap(
//Set spacing between chips horizontally
  spacing: 5.0,
//Set spacing between chips vertically
  runSpacing: 1.0,
  direction: Axis.horizontal,
  children: <Widget>[
    Image.asset(
      'images/scenes/BreakingBad1.jpg',
      height: 200,
      width: 300,
    ),
    Image.asset(
      'images/scenes/BreakingBad2.jpg',
      height: 200,
      width: 300,
    ),
    Image.asset(
      'images/scenes/BreakingBad3.jpg',
      height: 200,
      width: 300,
    ),
    Image.asset(
      'images/scenes/BreakingBad4.jpg',
      height: 200,
      width: 300,
    ),
  ],
);

//Display actors/actresses and their role
Widget peopleWrap = Wrap(
//Set spacing between chips horizontally
  spacing: 5.0,
//Set spacing between chips vertically
  runSpacing: 1.0,
  direction: Axis.horizontal,
  children: <Widget>[
    Column(
      children: [
        Image.asset(
          'images/people/Bryan Cranston.jpg',
          height: 200,
          width: 150,
        ),
        Container(
          width: 150,
          child: Text(
            'Bryan Cranston as Walter White',
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ),
    Column(
      children: [
        Image.asset(
          'images/people/Anna Gunn.jpg',
          height: 200,
          width: 150,
        ),
        Container(
          width: 150,
          child: Text(
            'Anna Gunn as Skyler White',
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ),
    Column(
      children: [
        Image.asset(
          'images/people/Aaron Paul.jpg',
          height: 200,
          width: 150,
        ),
        Container(
          width: 150,
          child: Text(
            'Aaron Paul as Jessie Pinkman',
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ),
    Column(
      children: [
        Image.asset(
          'images/people/Betsy Brandt.jpg',
          height: 200,
          width: 150,
        ),
        Container(
          width: 150,
          child: Text(
            'Betsy Brandt as Marie Schrader',
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ),
  ],
);

class BreakingBad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Breaking Bad"),
        ),
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
              child: Text(
                "TV-MA | 49min | Crime, Drama, Thriller | TV Series (2008–2013)",
                textAlign: TextAlign.center,
                style: subBodyText,
              ),
            ),
            Divider(),
            SingleChildScrollView(
              //Show scene images in a horizontal bar
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              scrollDirection: Axis.horizontal,
              child: scenesWrap,
            ),
            Container(
                padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
                child: Text(
                  "About",
                  style: sectionTitleText,
                )),
            Container(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Text(
                '           When chemistry teacher Walter White is diagnosed with'
                    ' Stage III cancer and given only two years to live, he decid'
                    'es he has nothing to lose. He lives with his teenage son, wh'
                    'o has cerebral palsy, and his wife, in New Mexico. Determine'
                    'd to ensure that his family will have a secure future, Walt '
                    'embarks on a career of drugs and crime. He proves to be rema'
                    'rkably proficient in this new world as he begins manufacturi'
                    'ng and selling methamphetamine with one of his former studen'
                    'ts. The series tracks the impacts of a fatal diagnosis on a '
                    'regular, hard working man, and explores how a fatal diagnosi'
                    's affects his morality and transforms him into a major playe'
                    'r of the drug trade. Show description written by WellardRock'
                    'ard and jackenyon',
                style: bodyText,
              ),
            ),
            SingleChildScrollView(
              //Show actors/actresses in a horizontal bar
              padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
              scrollDirection: Axis.horizontal,
              child: peopleWrap,
            ),
          ],
        ));
  }
}
